const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');

/*
 * SplitChunksPlugin is enabled by default and replaced
 * deprecated CommonsChunkPlugin. It automatically identifies modules which
 * should be splitted of chunk by heuristics using module duplication count and
 * module category (i. e. node_modules). And splits the chunks…
 *
 * It is safe to remove "splitChunks" from the generated configuration
 * and was added as an educational example.
 *
 * https://webpack.js.org/plugins/split-chunks-plugin/
 *
 */

const HtmlWebpackPlugin = require('html-webpack-plugin');

/*
 * We've enabled HtmlWebpackPlugin for you! This generates a html
 * page for you when you compile webpack, which will make you start
 * developing and prototyping faster.
 *
 * https://github.com/jantimon/html-webpack-plugin
 *
 */

module.exports = {
	mode: 'development',

	entry: {

		index: './src/index.js',
		persona: './src/persona.js',
		paciente: './src/paciente.js',
		spec: './src/spec.js',
		cuenta: './src/cuenta.js',
		turno: './src/turno.js',
		controllerRegister: './src/controllerRegister.js',
		indexDoctorRegistration: './src/indexDoctorRegistration.js',
		indexPatientRegistration: './src/indexPatientRegistration.js',
		horario: './src/horario.js',
		carreers: './src/carreers.js',
		admin: './src/admin.js',
		patientProfile: './src/patientProfile.js',
		doctorProfile: './src/doctorProfile.js',
		donaciones:'./src/donaciones.js',
		contact: './src/contact.js',
		aboutus: './src/aboutus.js'
	},

	output: {
		filename: '[name].[chunkhash].js',
		path: path.resolve(__dirname, 'dist')
	},

	plugins: [new webpack.ProgressPlugin(), 
		new HtmlWebpackPlugin({
            template: './src/indexPatientRegistration.html',
            chunks: ['indexPatientRegistration'],
            filename: 'indexPatientRegistration.html'
            }),
		new HtmlWebpackPlugin({
            template: './src/indexDoctorRegistration.html',
            chunks: ['indexDoctorRegistration'],
            filename: 'indexDoctorRegistration.html'
			}),
        new HtmlWebpackPlugin({
            template: './src/index.html',
            chunks: ['index'],
            filename: 'index.html'
			}),
        new HtmlWebpackPlugin({
            template: './src/spec.html',
            chunks: ['spec'],
            filename: 'spec.html'
			}),
        new CopyWebpackPlugin([{
            from:'./src/assets/images', 
            to:'assets/images'
        	}]),
        new HtmlWebpackPlugin({
            template: './src/carreers.html',
            chunks: ['carreers'],
            filename: 'carreers.html'
			}),
        new HtmlWebpackPlugin({
            template: './src/admin.html',
            chunks: ['admin'],
            filename: 'admin.html'
            }),
        new HtmlWebpackPlugin({
            template: './src/patientProfile.html',
            chunks: ['patientProfile'],
            filename: 'patientProfile.html'
			}),
        new HtmlWebpackPlugin({
            template: './src/doctorProfile.html',
            chunks: ['doctorProfile'],
            filename: 'doctorProfile.html'
			}),
         new HtmlWebpackPlugin({
            template: './src/donaciones.html',
            chunks: ['donaciones'],
            filename: 'donaciones.html'
			}),
        new HtmlWebpackPlugin({
            template: './src/contact.html',
            chunks: ['contact'],
            filename: 'contact.html'
			}),
        new HtmlWebpackPlugin({
            template: './src/aboutus.html',
            chunks: ['aboutus'],
            filename: 'aboutus.html'
			})
	],

	module: {
		rules: [
			{
				test: /.(js|jsx)$/,
				include: [path.resolve(__dirname, 'src/')],
				loader: 'babel-loader',

				options: {
					plugins: ['syntax-dynamic-import'],

					presets: [
						[
							'@babel/preset-env',
							{
								modules: false
							}
						]
					]
				}
			},
			{                 
			  test: [/.css$/],                
			  use:[                    
			   'style-loader',                  
			   'css-loader'
			  ]            
			},
			{
        test: /\.(png|jpg|gif|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'assets/images'
            }
          }
        ]
      }

		],
	},

	optimization: {
		splitChunks: {
			cacheGroups: {
				vendors: {
					priority: -10,
					test: /[\\/]node_modules[\\/]/
				}
			},

			chunks: 'async',
			minChunks: 1,
			minSize: 30000,
			name: true
		}
	},

	devServer: {
		open: true
	}
};
