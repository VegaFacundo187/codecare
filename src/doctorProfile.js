import '../src/profile.css'
import Turno from './turno';
import moment from 'moment';

let tablaDelDia = document.getElementById('docTable');

let turnos = JSON.parse(localStorage.getItem('turnos')) || [];

tablaDelDia.innerHTML ="";

arryOrdenado().forEach( function(elementTurno, index) {
	
	let doctor = JSON.parse(localStorage.getItem('tempUsser')) || [] ;
	let pacientes = JSON.parse(localStorage.getItem('accountsPatients')) || [] 

	if(doctor.numeroDeCuenta == elementTurno.idDoctor && (elementTurno.fecha > moment().format('YYYY/MM/DD'))){

		let paciente = pacientes.find(function(elementPatient){

			return elementPatient.numeroDeCuenta == elementTurno.idPatient;
		})

		tablaDelDia.innerHTML += `<td>${paciente.dataUser.name} ${paciente.dataUser.lastname}</td>
		<td>${moment(elementTurno.fecha).format('DD/MM/YYYY')}-${elementTurno.hora}</td>
		<td><p>${elementTurno.descripcion}</p></td>
		</tr>`;
	}

});

function arryOrdenado(){

	let turnos = JSON.parse(localStorage.getItem('turnos')) || [];
	let tempArrayTurnos  = [];

	turnos.forEach( function(elementTurno) {

		let doctor = JSON.parse(localStorage.getItem('tempUsser')) || [] ;
		let pacientes = JSON.parse(localStorage.getItem('accountsPatients')) || [] 

		if(doctor.numeroDeCuenta == elementTurno.idDoctor){

			tempArrayTurnos.push(elementTurno);

		}

	});

	tempArrayTurnos.sort(function(a,b){

		return a.fecha<b.fecha  ? -1: a.fecha>b.fecha ? 1 : 0;

	});

	return tempArrayTurnos;
}  

let nameDoctor = document.getElementById('doctName');
let tblDoctor = document.getElementById('datosDoctor');

let doctor = JSON.parse(localStorage.getItem('tempUsser')) || [];
let dtsDoctor = document.getElementById('datosDoctor');

nameDoctor.innerHTML = `<h3 class="text-center typo mb-1">${doctor.dataUser.name} ${doctor.dataUser.lastname}</h3>`


tblDoctor.innerHTML = `           
<tr>
<td scope="col"><strong>DNI</strong></td>
<td scope="col">${doctor.dataUser.dni}</td>
</tr>
<tr>
<td scope="col"><strong>ADDRESS</strong></td>
<td scope="col">${doctor.dataUser.city}</td>
</tr>
<tr>
<td scope="col"><strong>E-MAIL</strong></td>
<td scope="col">${doctor.mail}</td>
</tr>
<tr>
<td scope="col"><strong>TELEPHONE</strong></td>
<td scope="col">${doctor.dataUser.telefono}</td>
</tr>
`



//-------Cargar el logo de usuario registrado-------

function agregarLogodDeLogueo(){

	let log = document.getElementById('logo-log');

	let use = JSON.parse(localStorage.getItem('tempUsser'));

	if(use != undefined){


		
		
		if(use.tipo == "Patient"){

			log.classList.add('d-none');
			log.classList.add('d-md-block');
			log.innerHTML = `<a class="btn dropdown-toggle-modificado" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
			<i class="far fa-user fa-2x"></i>
			</a>
			<div class="dropdown-menu dropdown-menu-modificado dropdown-menu-right" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item" id="" href="patientProfile.html">Profile</a>
			<a class="dropdown-item" id="deslog1"href="index.html">Logout</a>
			</div>`;

			log.insertAdjacentHTML('afterend',`<a id="deslog2" class="nav-link btn nav-care-btn pb-1 d-sm-block d-md-none" href="" style="background-color: #227a66">
				Logout
				</a>`);
			log.insertAdjacentHTML('afterend',`<a class="nav-link btn nav-care-btn pb-1 d-sm-block d-md-none" href="patientProfile.html" style="background-color: #227a66">
				Profile
				</a>`);
		}

		if(use.tipo == "Doctor"){


			log.classList.add('d-none');
			log.classList.add('d-md-block');
			log.innerHTML = `<a class="btn dropdown-toggle-modificado" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
			<i class="far fa-user fa-2x"></i>
			</a>
			<div class="dropdown-menu dropdown-menu-modificado dropdown-menu-right" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item" id="" href="doctorProfile.html">Profile</a>
			<a class="dropdown-item" id="deslog1" href="index.html">Logout</a>
			</div>`;

			log.insertAdjacentHTML('afterend',`<a id="deslog2"class="nav-link btn nav-care-btn pb-1 d-sm-block d-md-none" href="index.html" style="background-color: #227a66">
				Logout
				</a>`);
			log.insertAdjacentHTML('afterend',`<a class="nav-link btn nav-care-btn pb-1 d-sm-block d-md-none" href="doctorProfile.html" style="background-color: #227a66">
				Profile
				</a>`);
		}


	}
	else{
		console.log(use);
		log.innerHTML = `<a class="nav-link btn nav-care-btn pb-1" href="#" data-toggle="modal" data-target=".bd-example-modal-lg">
		Login/Register
		</a>`;
	}

	deslogueo();


}

//DESLOGUEO POR F.GRAMAJO
function deslogueo(){

	let d1 = document.getElementById('deslog1');
	let d2 = document.getElementById('deslog2');

	if(d1 != undefined){
		d1.addEventListener('click',function(){

			localStorage.removeItem('tempUsser');
			location.reload();
		});
	}

	if(d2 != undefined){
		d2.addEventListener('click',function(){

			localStorage.removeItem('tempUsser');
			location.reload();
		});
	}
}

agregarLogodDeLogueo();


