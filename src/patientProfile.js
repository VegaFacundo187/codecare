import '../src/profile.css';
import Turno from './turno';
import moment from 'moment';


let tablaDelDia = document.getElementById('tblDay');

let turnos = JSON.parse(localStorage.getItem('turnos')) || [];

tablaDelDia.innerHTML ="";

let turnosOrdenados =  arryOrdenado();

turnosOrdenados.forEach( function(elementTurno, index) {
	
	let actUser = JSON.parse(localStorage.getItem('tempUsser')) || [];


	if((actUser.numeroDeCuenta == elementTurno.idPatient) && (elementTurno.fecha > moment().format('YYYY/MM/DD'))){


		let doctores = JSON.parse(localStorage.getItem('accountsDoctors')) || [];
		let doctor = doctores.find(function(elementDoctor){

			return elementDoctor.numeroDeCuenta == elementTurno.idDoctor;
		})


		tablaDelDia.innerHTML += `<tr>  
		<td>${doctor.dataUser.name}</td>
		<td>${moment(elementTurno.fecha).format('DD/MM/YYYY')}-${elementTurno.hora}</td>
		<td>${doctor.dataUser.speciality}</td>
		</tr>`;
	}
});



function arryOrdenado(){

	let turnos = JSON.parse(localStorage.getItem('turnos')) || [];
	let tempArrayTurnos  = [];

	turnos.forEach( function(elementTurno, index) { 

		let actUser = JSON.parse(localStorage.getItem('tempUsser')) || [];

		if(actUser.numeroDeCuenta == elementTurno.idPatient){
			
			tempArrayTurnos.push(elementTurno);

		}
	});

	tempArrayTurnos.sort(function(a,b){

		return a.fecha<b.fecha  ? -1: a.fecha>b.fecha ? 1 : 0;

	});

	return tempArrayTurnos;

}

let namePatient = document.getElementById('nombrePaciente');

let paciente = JSON.parse(localStorage.getItem('tempUsser')) || [];
let dtsPaciente = document.getElementById('datosPaciente');

namePatient.innerHTML = `<h3 class="text-center typo mb-1">${paciente.dataUser.name} ${paciente.dataUser.lastname}</h3>`

dtsPaciente.innerHTML = `<tr>
<td scope="col"><strong>DNI</strong></td>
<td scope="col">${paciente.dataUser.dni}</td>
</tr>
<tr>
<td scope="col"><strong>ADDRESS</strong></td>
<td scope="col">${paciente.dataUser.domicilio}</td>
</tr>
<tr>
<td scope="col"><strong>E-MAIL</strong></td>
<td scope="col">${paciente.mail}</td>
</tr>
<tr>
<td scope="col"><strong>PREPAID</strong></td>
<td scope="col">${paciente.dataUser.obraSocial}</td>
</tr>
<tr>
<td scope="col"><strong>TELEPHONE</strong></td>
<td scope="col">${paciente.dataUser.telefono}</td>
</tr>`

// MODAL TURNOS

$('#datepicker').datepicker({todayHighlight:true});
$('#datepicker').on('changeDate', function() {
	$('#my_hidden_input').val($('#datepicker').datepicker('getFormattedDate'));
	let x = ($('#datepicker').datepicker('getFormattedDate'));
	$('#inputDate').text(moment(x).format('YYYY/MM/DD'));
	cargarHoraAgain();
});

//---------LLERNAR DESPLEGABLE DE MEDICOS EN EL MODAL DE JJ--------

function fillDoctors(){

	let desplegable = document.getElementById('inputState');

	let doctors = JSON.parse(localStorage.getItem('accountsDoctors')) || [];

	desplegable.innerHTML += `<option>Choose a doctor </option> `;

	doctors.forEach( function(element, index) {
		
		desplegable.innerHTML += `<option id="${element.numeroDeCuenta}">${element.dataUser.name} ${element.dataUser.lastname} (${element.dataUser.speciality})</option> `;
	});
}

fillDoctors();

//---------LLERNAR DESPLEGABLE DE MEDICOS EN EL MODAL DE JJ--------


//----------LLENAR LAS HORAS DE ATENCIÓN EN EL MODAL DE JJ------------


function llenarHorasAtención(){

	let doctors = JSON.parse(localStorage.getItem('accountsDoctors')) || [];

	let doctorsDesplegable = document.getElementById('inputState');

	doctorsDesplegable.addEventListener('change',function(){

		let getDoctor = document.getElementById('inputState');

		let doc = doctors.find(function(element){

			return ( element.numeroDeCuenta == getDoctor[getDoctor.selectedIndex].id);


		});



		let form = document.getElementById('hoursAtention');
		form.innerHTML ='';

		let fullScheduleDoc = [];
		fullScheduleDoc = doc.dataUser.schedule;


		let oneDayDoc = fullScheduleDoc.find(function(element){

			return element.fecha == document.getElementById('inputDate').textContent;		


		});



		oneDayDoc.hora.forEach( function(element, index) {

			form.innerHTML += ` <label id="btnhour${index}" class="btn btn-outline-success m-1">
			<input type="radio" name="options" id="option${index}" value="${element}" autocomplete="off" checked>${element}
			</label>`	

			setTimeout(function(){

				let btn = document.getElementById(`btnhour${index}`);

				btn.addEventListener('click',function(){


					document.getElementById('hourSelected').textContent = document.getElementById(`option${index}`).value;

				});


			},0);

		});

	})

}

llenarHorasAtención();

//----------LLENAR LAS HORAS DE ATENCIÓN EN EL MODAL DE JJ----------------


//-------------CREAMOS UN TURNO----------------


function iniciarCreacionTurno(){

	let btnturn = document.getElementById('btnConfirmTurno');

	btnturn.addEventListener('click',function(){



		let use = JSON.parse(localStorage.getItem('tempUsser'));

		if(use != undefined){

			let turno = crearTurno();

			let getDoctor = document.getElementById('inputState');

			let doc = doctors.find(function(element){

				return ( element.numeroDeCuenta == getDoctor[getDoctor.selectedIndex].id);


			});
		}
		else{
			alert('Debe loguearse');
		}


	})
}

iniciarCreacionTurno();

function crearTurno(){


	let fecha = document.getElementById('inputDate').textContent;
	let hora =  document.getElementById('hourSelected').textContent;
	let descripcion = document.getElementById('reasonsTextarea').value;

	let doctors = JSON.parse(localStorage.getItem('accountsDoctors')) || [];

	let doctorsDesplegable = document.getElementById('inputState');
	let getDoctor = document.getElementById('inputState');
	let userPatientLog = JSON.parse(localStorage.getItem('tempUsser')) || [];
	let doc = doctors.find(function(element){

		return ( element.numeroDeCuenta == getDoctor[getDoctor.selectedIndex].id);
		

	});

	debugger
	
	if(fecha=="" || hora == "" || descripcion == ""){
		alert('Fields empty');

	}
	else{

		

		let turno = new Turno(fecha,hora,descripcion);
		turno.idDoctor = doc.numeroDeCuenta;
		turno.idPatient = userPatientLog.numeroDeCuenta;
		let turnosContainer = JSON.parse(localStorage.getItem('turnos')) || [];
		turnosContainer.push(turno);
		localStorage.setItem('turnos', JSON.stringify(turnosContainer));

		borrarHoraTurno(hora,fecha); //lamamos a esta función para borrar el día que se acaba de usar en el turno	

		alert('An appointment was created succesfully');

		location.reload();

		return ;	

	}

}
//-------------CREAMOS UN TURNO----------------



// ------------FUNCIONES UTILIZADAS EN CREAR TURNO-----------
function borrarHoraTurno(hora,fecha){

	//obtengo un doctor
	let getDoctor = document.getElementById('inputState');


	let doctores = JSON.parse(localStorage.getItem('accountsDoctors')) || [];


	let doc = doctores.find(function(element){

		return ( element.numeroDeCuenta == getDoctor[getDoctor.selectedIndex].id);
		

	});

	//obtengo el índice de esa fecha

	//obtengo el indice del doc
	let idDoc = doctores.findIndex(function(element){ return element.numeroDeCuenta == getDoctor[getDoctor.selectedIndex].id});

	//obtengo el indice de un elemento de la colección schedule
	let idFech = doctores[idDoc].dataUser.schedule.findIndex(function(element){return element.fecha == fecha });

	//obtengo el indice de un elemento de la colección hora
	let idhora = doctores[idDoc].dataUser.schedule[idFech].hora.findIndex(function(element){return element == hora});

	//borro esa hora de la coleción 
	doctores[idDoc].dataUser.schedule[idFech].hora.splice(idhora,1);

	//guardo en localStorage
	localStorage.setItem('accountsDoctors', JSON.stringify(doctores));

	//VOLVEMOS A LLENAR EL DESPLEGAB

	cargarHoraAgain();

}


function cargarHoraAgain(){


	let doct = JSON.parse(localStorage.getItem('accountsDoctors')) || [];

	// console.log('losDocsson: ')
	// console.log(doctores);

	let getDoctor = document.getElementById('inputState');

	let doc = doct.find(function(element){

		return ( element.numeroDeCuenta == getDoctor[getDoctor.selectedIndex].id);


	});



	let form = document.getElementById('hoursAtention');
	form.innerHTML ='';

	let fullScheduleDoc = [];
	fullScheduleDoc = doc.dataUser.schedule;


	let oneDayDoc = fullScheduleDoc.find(function(element){

		return element.fecha == document.getElementById('inputDate').textContent;		


	});



	oneDayDoc.hora.forEach( function(element, index) {

		form.innerHTML += ` <label id="btnhour${index}" class="btn btn-outline-success m-1">
		<input type="radio" name="options" id="option${index}" value="${element}" autocomplete="off" checked>${element}
		</label>`	

		setTimeout(function(){

			let btn = document.getElementById(`btnhour${index}`);

			btn.addEventListener('click',function(){


				document.getElementById('hourSelected').textContent = document.getElementById(`option${index}`).value;

			});


		},0);

	});


}

// ------------FUNCIONES UTILIZADAS EN CREAR TURNO-----------

//-------Cargar el logo de usuario registrado-------

function agregarLogodDeLogueo(){

	let log = document.getElementById('logo-log');

	let use = JSON.parse(localStorage.getItem('tempUsser'));

	if(use != undefined){


		
		if(use.tipo == "Patient"){

			log.classList.add('d-none');
			log.classList.add('d-md-block');
			log.innerHTML = `<a class="btn dropdown-toggle-modificado" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
			<i class="far fa-user fa-2x"></i>
			</a>
			<div class="dropdown-menu dropdown-menu-modificado dropdown-menu-right" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item" id="" href="patientProfile.html">Profile</a>
			<a class="dropdown-item" id="deslog1"href="index.html">Logout</a>
			</div>`;

			log.insertAdjacentHTML('afterend',`<a id="deslog2" class="nav-link btn nav-care-btn pb-1 d-sm-block d-md-none" href="" style="background-color: #227a66">
				Logout
				</a>`);
			log.insertAdjacentHTML('afterend',`<a class="nav-link btn nav-care-btn pb-1 d-sm-block d-md-none" href="patientProfile.html" style="background-color: #227a66">
				Profile
				</a>`);
		}

		if(use.tipo == "Doctor"){


			log.classList.add('d-none');
			log.classList.add('d-md-block');
			log.innerHTML = `<a class="btn dropdown-toggle-modificado" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
			<i class="far fa-user fa-2x"></i>
			</a>
			<div class="dropdown-menu dropdown-menu-modificado dropdown-menu-right" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item" id="" href="doctorProfile.html">Profile</a>
			<a class="dropdown-item" id="deslog1" href="index.html">Logout</a>
			</div>`;

			log.insertAdjacentHTML('afterend',`<a id="deslog2"class="nav-link btn nav-care-btn pb-1 d-sm-block d-md-none" href="index.html" style="background-color: #227a66">
				Logout
				</a>`);
			log.insertAdjacentHTML('afterend',`<a class="nav-link btn nav-care-btn pb-1 d-sm-block d-md-none" href="doctorProfile.html" style="background-color: #227a66">
				Profile
				</a>`);
		}


	}
	else{
		console.log(use);
		log.innerHTML = `<a class="nav-link btn nav-care-btn pb-1" href="#" data-toggle="modal" data-target=".bd-example-modal-lg">
		Login/Register
		</a>`;
	}

	deslogueo();

}

//DESLOGUEO POR F.GRAMAJO
function deslogueo(){

	let d1 = document.getElementById('deslog1');
	let d2 = document.getElementById('deslog2');

	if(d1 != undefined){
		d1.addEventListener('click',function(){

			localStorage.removeItem('tempUsser');
			location.reload();
		});
	}

	if(d2 != undefined){
		d2.addEventListener('click',function(){

			localStorage.removeItem('tempUsser');
			location.reload();
		});
	}
}

agregarLogodDeLogueo();





