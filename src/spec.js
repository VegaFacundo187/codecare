import '../src/style.css';
import '../src/spec.css';


//import Specializations from './spec.js';
function loadSpe(){

	let btnSpe = document.getElementById('btnSpe');

	btnSpe.innerHTML="";

	btnSpe.innerHTML = `<button id="spe-1" type="button " class="dropdown-item">Traumatology</button>
              	<button id="spe-2" type="button " class="dropdown-item">Pediatrics</button>	
              	<button id="spe-3" type="button " class="dropdown-item">Urology</button>`;


/*
     if(true){
			setTimeout(function(){
			    let spe1 = document.getElementById(`spe-1`);
				spe1.addEventListener('click', function(){
				putSpe();
				});
				let spe2 = document.getElementById(`spe-2`);
				spe2.addEventListener('click', function(){
				putSpe2();
				});
				let spe3 = document.getElementById(`spe-3`);
				spe3.addEventListener('click', function(){
				putSpe3();
				});

				
			}, 0);
		}*/	
			    var spe1 = document.getElementById(`spe-1`);
				spe1.addEventListener('click', function(){
				putSpe1();
				});
				var spe2 = document.getElementById(`spe-2`);
				spe2.addEventListener('click', function(){
				putSpe2();
				});
				var spe3 = document.getElementById(`spe-3`);
				spe3.addEventListener('click', function(){
				putSpe3();
				});



}	

function putSpe1(){
	let table = document.getElementById('table');
	table.innerHTML="";
	table.innerHTML=`<h2>Traumatology</h2>
				<table class="table table-bordered ">
				  <thead>
				    <tr>
				      <th scope="col">Drs.</th>

				    </tr>
				  </thead>
				  <tbody>
				  	<tr>
					     <th scope="col">#</th>
					     <th scope="col">Dr. Juan Perez</th>
					 </tr>
				    <tr>
					  
				      <th scope="row">Schedules</th>
				      <td>Sunday</td>
				      <td>Monday</td>
				      <td>Tuesday</td>
				      <td>Wednesday</td>
				      <td>Thursday</td>
				      <td>Friday</td>
				      <td>Saturday</td>
				    </tr>
				    <tr>
				      <th scope="row"></th>
				      <td>8:30hs - 16:00hs</td>
				      <td>8:30hs - 16:00hs</td>
				      <td>8:30hs - 16:00hs</td>
				      <td>8:30hs - 16:00hs</td>
				      <td>8:30hs - 16:00hs</td>
				      <td>Off</td>
				      <td>Off</td>
				    </tr>
				  </tbody>
				</table>`;
}

loadSpe();

function putSpe2(){
	let table = document.getElementById('table');
	table.innerHTML="";
	table.innerHTML=`<h2>Pediatrics</h2>
				<table class="table table-bordered ">
				  <thead>
				    <tr>
				      <th scope="col">Drs.</th>

				    </tr>
				  </thead>
				  <tbody>
				  	<tr>
					     <th scope="col">#</th>
					     <th scope="col">Dr. Juliana Mendez</th>
					 </tr>
				    <tr>
					  
				      <th scope="row">Schedules</th>
				      <td>Sunday</td>
				      <td>Monday</td>
				      <td>Tuesday</td>
				      <td>Wednesday</td>
				      <td>Thursday</td>
				      <td>Friday</td>
				      <td>Saturday</td>
				    </tr>
				    <tr>
				      <th scope="row"></th>
				      <td>Off</td>
				      <td>10:00hs - 18:00hs</td>
				      <td>10:00hs - 18:00hs</td>
				      <td>10:00hs - 18:00hs</td>
				      <td>10:00hs - 18:00hs</td>
				      <td>10:00hs - 18:00hs</td>
				      <td>Off</td>
				    </tr>
				  </tbody>
				</table>`;

}


function putSpe3(){
	let table = document.getElementById('table');
	table.innerHTML="";
	table.innerHTML=`<h2>Urology</h2>
				<table class="table table-bordered ">
				  <thead>
				    <tr>
				      <th scope="col">Drs.</th>

				    </tr>
				  </thead>
				  <tbody>
				  	<tr>
					     <th scope="col">#</th>
					     <th scope="col">Dr. Horacio Gomez</th>
					 </tr>
				    <tr>
					  
				      <th scope="row">Schedules</th>
				      <td>Sunday</td>
				      <td>Monday</td>
				      <td>Tuesday</td>
				      <td>Wednesday</td>
				      <td>Thursday</td>
				      <td>Friday</td>
				      <td>Saturday</td>
				    </tr>
				    <tr>
				      <th scope="row"></th>
				      <td>Off</td>
				      <td>14:00hs - 22:00hs</td>
				      <td>14:00hs - 22:00hs</td>
				      <td>14:00hs - 22:00hs</td>
				      <td>14:00hs - 22:00hs</td>
				      <td>14:00hs - 22:00hs</td>
				      <td>Off</td>
				    </tr>
				  </tbody>
				</table>`;
}




// CODIGO PARA EL NAV Y BOTON DE LOGUEO

import Doctor from './doctor';
import Paciente from './paciente';
import Turno from './turno';

let logContainer = document.getElementById('loginSingup');


document.getElementById('singupbtn').addEventListener('click',function(){
	
	clickSingUp();
})

document.getElementById('loginbtn').addEventListener('click',function(){

	clickLoging();
})

function clickLoging(){

	logContainer.style.transform = 'translate(0px)';
	document.getElementById('singup').style.transform = 'translateY(-120%)';
	document.getElementById('singupbtn').setAttribute('disabled',true);
	disableButton(document.getElementById('singupbtn'));
	

}


function clickSingUp(){


	logContainer.style.transform = 'translate(102%)';
	document.getElementById('loginSingup').style.zIndex = '2';
	document.getElementById('singup').classList.remove("invisible");
	document.getElementById('singup').style.transform = 'translateY(120%)';
	document.getElementById('loginbtn').setAttribute('disabled',true);
	disableButton(document.getElementById('loginbtn'));
	
}

function disableButton(btn){

	setTimeout(function(){
		btn.removeAttribute('disabled');
	},1000);


}

// MODAL LOGIN RESPONSIVE


document.getElementById('btn-responsive-modal-doctor').addEventListener('click', function(){

	clickResponsiveDoctor();
})


let docResponsive = document.getElementById('login-resoponsive-doctor');

function clickResponsiveDoctor(){

	docResponsive.style.transform = 'translate(-110%)';
	docResponsive.style.zIndex = '1';

}

document.getElementById('btn-responsive-modal-patient').addEventListener('click', function(){

	clickResponsivePtient();
});

let patResponsive = document.getElementById('login-resoponsive-patient');

function clickResponsivePtient(){

	patResponsive.style.transform = 'translate(110%)';
	patResponsive.style.zIndex = '1';

} 


document.getElementById('return-from-patient').addEventListener('click',function(){

	returnFromPatient();

});

function returnFromPatient(){

	patResponsive.style.transform = 'translate(-110%)';
	patResponsive.style.zIndex = '-1';

}

document.getElementById('return-from-doctor').addEventListener('click',function(){

	returnFromDoctor();

});

function returnFromDoctor(){

	docResponsive.style.transform = 'translate(110%)';
	docResponsive.style.zIndex = '-1';
}

//-------Cargar el logo de usuario registrado-------

function agregarLogodDeLogueo(){

	let log = document.getElementById('logo-log');

	let use = JSON.parse(localStorage.getItem('tempUsser'));

	if(use != undefined){


		
		if(use.tipo == "Patient"){

			log.classList.add('d-none');
			log.classList.add('d-md-block');
			log.innerHTML = `<a class="btn dropdown-toggle-modificado" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
			<i class="far fa-user fa-2x"></i>
			</a>
			<div class="dropdown-menu dropdown-menu-modificado dropdown-menu-right" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item" id="" href="patientProfile.html">Profile</a>
			<a class="dropdown-item" id="deslog1"href="#">Logout</a>
			</div>`;

			log.insertAdjacentHTML('afterend',`<a id="deslog2" class="nav-link btn nav-care-btn pb-1 d-sm-block d-md-none" href="" style="background-color: #227a66">
				Logout
				</a>`);
			log.insertAdjacentHTML('afterend',`<a class="nav-link btn nav-care-btn pb-1 d-sm-block d-md-none" href="patientProfile.html" style="background-color: #227a66">
				Profile
				</a>`);
		}

		if(use.tipo == "Doctor"){


			log.classList.add('d-none');
			log.classList.add('d-md-block');
			log.innerHTML = `<a class="btn dropdown-toggle-modificado" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
			<i class="far fa-user fa-2x"></i>
			</a>
			<div class="dropdown-menu dropdown-menu-modificado dropdown-menu-right" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item" id="" href="doctorProfile.html">Profile</a>
			<a class="dropdown-item" id="deslog1" href="#">Logout</a>
			</div>`;

			log.insertAdjacentHTML('afterend',`<a id="deslog2"class="nav-link btn nav-care-btn pb-1 d-sm-block d-md-none" href="" style="background-color: #227a66">
				Logout
				</a>`);
			log.insertAdjacentHTML('afterend',`<a class="nav-link btn nav-care-btn pb-1 d-sm-block d-md-none" href="doctorProfile.html" style="background-color: #227a66">
				Profile
				</a>`);
		}


	}
	else{
		console.log(use);
		log.innerHTML = `<a class="nav-link btn nav-care-btn pb-1" href="#" data-toggle="modal" data-target=".bd-example-modal-lg">
		Login/Register
		</a>`;
	}

	deslogueo();


}

//DESLOGUEO POR F.GRAMAJO
function deslogueo(){

	let d1 = document.getElementById('deslog1');
	let d2 = document.getElementById('deslog2');

	if(d1 != undefined){
		d1.addEventListener('click',function(){
	
			localStorage.removeItem('tempUsser');
			location.reload();
		});
	}

	if(d2 != undefined){
		d2.addEventListener('click',function(){

			localStorage.removeItem('tempUsser');
			location.reload();
		});
	}
}



function agregarEventosBotonesLogoLogueo(){

	agregarLogodDeLogueo();

	let botP = document.getElementById('ptLogIn');

	botP.addEventListener('click',function(){

		agregarLogodDeLogueo();

	});

	let botD = document.getElementById('drLogIn');

	botD.addEventListener('click',function(){

		agregarLogodDeLogueo();

	});

	// Botones en responsive del modal al que se agregan eventos para cambiar el boton de logueo

	let botPr = document.getElementById('drLogIn1');

	botPr.addEventListener('click',function(){

		agregarLogodDeLogueo();

	});

	let botPp = document.getElementById('ptLogIn1');

	botPp.addEventListener('click',function(){

		agregarLogodDeLogueo();

	});
}



agregarEventosBotonesLogoLogueo();


	            