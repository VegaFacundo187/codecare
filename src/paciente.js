import Persona from './persona';
export default class Paciente extends Persona{

	constructor(name,lastname,dni,date,gender,city,obraSocial,telefono){
		super(name,lastname,dni,date,gender,city);
		this.obraSocial = obraSocial;
		this.telefono = telefono;
				
	}
}