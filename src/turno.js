export default class Turno{

	constructor(fecha,hora,descripcion){
		this.fecha = fecha;
		this.hora = hora;
		this.descripcion = descripcion; //Motivo de visita
		this.idDoctor = "";
		this.idPatient = "";
	}
}