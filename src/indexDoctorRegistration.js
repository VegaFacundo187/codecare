import ControllerRegister from './controllerRegister';
import '../src/indexPatientRegistration.css';

let controlerR = new ControllerRegister();
let todoEnOrden = false;

// Fetch all the forms we want to apply custom Bootstrap validation styles to
var forms = document.getElementsByClassName('needs-validation');
// Loop over them and prevent submission
var validation = Array.prototype.filter.call(forms, function(form) {
  form.addEventListener('submit', function(event) {
  	repeatedPassOk();
  	if (form.checkValidity() === false || todoEnOrden == false) {
  	  event.preventDefault();
  	  event.stopPropagation();
  	}
  	else {
  		controlerR.createDoctor();
  	}
  	form.classList.add('was-validated');
  	repeatedPassOk();
  	document.getElementById('inputPasswordRepeat').addEventListener("input", repeatedPassOk);
  }, false);
});

function repeatedPassOk() {
	let password = document.getElementById('inputPassword');
	let reppassword = document.getElementById('inputPasswordRepeat');
	console.log(document.getElementById('inputPasswordRepeat').value);
	if (document.getElementById('inputPasswordRepeat').value != document.getElementById('inputPassword').value) {
		console.log('un string cualquiera');
		reppassword.setCustomValidity("Password doesn't match");
		$('#inputPasswordRepeatFeed').removeClass("d-none");
		$('#inputPasswordRepeatFeed').text("Password doesn't match");
	}
	else {
		document.getElementById('inputPasswordRepeat').setCustomValidity("");
		console.log("entra al else");
		console.log(forms[0].checkValidity());
		if (forms[0].checkValidity() == true) {
			todoEnOrden = true;
		}
	}
}
